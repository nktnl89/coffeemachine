package com.epam.training.spring.core.demo.DTO;

import java.util.HashMap;
import java.util.Map;

public class CoffeeMachine {

    Map<Drink, Integer> drinks = new HashMap<>();
    Map<Ingridient, Integer> ingridients = new HashMap<>();

    public CoffeeMachine(HashMap<Drink, Integer> drinks, HashMap<Ingridient, Integer> ingridients) {
        this.drinks = drinks;
        this.ingridients = ingridients;
    }

    public Map<Drink, Integer> getDrinks() {
        return drinks;
    }

    public void setDrinks(Map<Drink, Integer> drinks) {
        this.drinks = drinks;
    }

    public Map<Ingridient, Integer> getIngridients() {
        return ingridients;
    }

    public void setIngridients(Map<Ingridient, Integer> ingridients) {
        this.ingridients = ingridients;
    }


}
