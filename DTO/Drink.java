package com.epam.training.spring.core.demo.DTO;

import java.util.ArrayList;
import java.util.List;

public class Drink implements DrinkInterface {
    String name;
    List<Ingridient> ingridients = new ArrayList<>();

    public Drink(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Ingridient> getIngridients() {
        return ingridients;
    }

    public void setIngridients(List<Ingridient> ingridients) {
        this.ingridients = ingridients;
    }

    @Override
    public String toString() {
        String result = name + (ingridients.size() > 0 ? ", ingridients: " : "");
        for (Ingridient ingridient : ingridients) {
            result = result + ingridient.getName();
        }
        return result;
    }

    @Override
    public void addIngridient(Ingridient ingridient) {
        ingridients.add(ingridient);
    }
}
