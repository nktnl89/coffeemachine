package com.epam.training.spring.core.demo.DTO;

import java.util.ArrayList;
import java.util.List;

public class Account implements AccountInterface {
    List<Drink> drinks = new ArrayList<>();

    public Account() {
        this.drinks = drinks;
    }

    public List<Drink> getDrinks() {
        return drinks;
    }

    public void setDrinks(List<Drink> drinks) {
        this.drinks = drinks;
    }

    @Override
    public void addDrink(Drink drink) {
        drinks.add(drink);
    }
}
