package com.epam.training.spring.core.demo.DTO;

import org.springframework.stereotype.Component;

public class Ingridient {
    String name;

    public Ingridient(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}
