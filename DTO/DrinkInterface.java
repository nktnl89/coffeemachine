package com.epam.training.spring.core.demo.DTO;

public interface DrinkInterface {
    void addIngridient(Ingridient ingridient);
}
