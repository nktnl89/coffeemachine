package com.epam.training.spring.core.demo.repository.ingridients;

import com.epam.training.spring.core.demo.DTO.Ingridient;

import java.util.List;

public interface IngridientDAOInterface {
    void addIngridient(Ingridient ingridient);
    Ingridient getIngridient(String name);
    List<Ingridient> getAllIngridients();
}
