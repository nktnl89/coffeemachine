package com.epam.training.spring.core.demo.repository.ingridients;

import com.epam.training.spring.core.demo.DTO.Ingridient;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component("IngridientDAO")
public class IngridientDAO implements IngridientDAOInterface {
    List<Ingridient> ingridients = new ArrayList<>();

    @Override
    public void addIngridient(Ingridient ingridient) {
        ingridients.add(ingridient);
    }

    @Override
    public Ingridient getIngridient(String name) {
        Ingridient result = null;
        for (Ingridient ingridient : ingridients) {
            if (ingridient.getName() == name) {
                result = ingridient;
                return result;
            }
        }
        return result;
    }

    @Override
    public List<Ingridient> getAllIngridients() {
        return ingridients;
    }


}
