package com.epam.training.spring.core.demo.repository.accounts;

import com.epam.training.spring.core.demo.DTO.Account;
import com.epam.training.spring.core.demo.DTO.CoffeeMachine;
import com.epam.training.spring.core.demo.DTO.Drink;
import com.epam.training.spring.core.demo.DTO.Ingridient;
import com.epam.training.spring.core.demo.repository.coffeeMachines.CoffeeMachineDAO;
import com.epam.training.spring.core.demo.repository.coffeeMachines.CoffeeMachineDAOInterface;
import com.epam.training.spring.core.demo.repository.drinks.DrinkDAO;
import com.epam.training.spring.core.demo.repository.drinks.DrinkDAOInterface;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Component("AccountDAO")
public class AccountDAO implements AccountDAOInterface {
    List<Account> accounts = new ArrayList<>();

    @Override
    public Account addAccount() {
        Account newAccount = new Account();
        accounts.add(newAccount);
        return newAccount;
    }

    @Override
    public Account getAccount(Integer accountID) {
        return accounts.get(accountID);
    }

    @Override
    public List<Drink> getAllAccountDrinks(Account account) {
        return account.getDrinks();
    }

    @Override
    public void addDrink(Account account, CoffeeMachine coffeeMachine, Drink drink) {
        CoffeeMachineDAOInterface tmpCoffeeMachineDAO = new CoffeeMachineDAO();
        DrinkDAOInterface tmpDrinkDAO = new DrinkDAO();

        Integer coffeeMachineDrinkPortions = tmpCoffeeMachineDAO.getDrinkPortion(coffeeMachine, drink);
        Integer accountDrink = getCountDrinkInAccount(account, drink);
        if ((coffeeMachineDrinkPortions - accountDrink) >= 0) {
            tmpCoffeeMachineDAO.setDrinkPortion(coffeeMachine, drink, --coffeeMachineDrinkPortions);
            account.addDrink(tmpDrinkDAO.addDrink(drink.getName()));
        } else {
            System.out.println("В кофемашине недостаточно выбранного напитка.");
        }
    }

    @Override
    public void addIngridient(Account account, CoffeeMachine coffeeMachine, Integer accountDrinkId, Ingridient ingredient) {
        CoffeeMachineDAOInterface tmpCoffeeMachineDAO = new CoffeeMachineDAO();
        Integer coffeeMachineIngridientPortions = tmpCoffeeMachineDAO.getIngridientPortion(coffeeMachine, ingredient);
        Integer accountIngridient = getCountIngridientInAccount(account, ingredient);
        if ((coffeeMachineIngridientPortions - accountIngridient) >= 0) {
            List<Drink> tmpDrinks = account.getDrinks();
            Drink tmpDrink = tmpDrinks.get(accountDrinkId);
            tmpDrink.addIngridient(ingredient);
            account.setDrinks(tmpDrinks);
            tmpCoffeeMachineDAO.setIngridientPortion(coffeeMachine, ingredient, --coffeeMachineIngridientPortions);
        } else {
            System.out.println("В кофемашине недостаточно выбранного ингридиента.");
        }
    }

    @Override
    public void deleteDrink(Account account, Drink drink) {
        List<Drink> tmpDrinks = account.getDrinks();
        for (Drink entryDrink : tmpDrinks) {
            if (entryDrink.equals(drink)) {
                tmpDrinks.remove(entryDrink);
                account.setDrinks(tmpDrinks);
                break;
            }
        }
    }

    @Override
    public Account checkout(CoffeeMachine coffeeMachine, Account account) {
        System.out.println("------------------");
        System.out.println("Заказ:");
        Map<Drink, Integer> tmpDrinks = coffeeMachine.getDrinks();
        Map<Ingridient, Integer> tmpIngridient = coffeeMachine.getIngridients();
        for (Drink drink : account.getDrinks()) {
            System.out.println(drink);
        }
        return account;
    }

    @Override
    public Integer getCountDrinkInAccount(Account account, Drink drink) {
        Integer countDrinkInAccount = 0;
        for (Drink entryDrink : account.getDrinks()) {
            if (entryDrink.getName() == drink.getName()) {
                countDrinkInAccount++;
            }
        }
        return countDrinkInAccount;
    }

    @Override
    public Integer getCountIngridientInAccount(Account account, Ingridient ingridient) {
        Integer countIngridientInAccount = 0;
        for (Drink entryDrink : account.getDrinks()) {
            for (Ingridient entryIngridient : entryDrink.getIngridients()) {
                if (entryIngridient.equals(ingridient)) {
                    countIngridientInAccount++;
                }
            }
        }
        return countIngridientInAccount;
    }
}
