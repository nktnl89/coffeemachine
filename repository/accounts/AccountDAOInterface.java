package com.epam.training.spring.core.demo.repository.accounts;

import com.epam.training.spring.core.demo.DTO.Account;
import com.epam.training.spring.core.demo.DTO.CoffeeMachine;
import com.epam.training.spring.core.demo.DTO.Drink;
import com.epam.training.spring.core.demo.DTO.Ingridient;

import java.util.List;

public interface AccountDAOInterface {
    Account addAccount();

    Account getAccount(Integer accountID);

    List<Drink> getAllAccountDrinks(Account account);

    void addDrink(Account account, CoffeeMachine coffeeMachine, Drink drink);

    void addIngridient(Account account, CoffeeMachine coffeeMachine, Integer accountDrinkId, Ingridient ingredient);

    void deleteDrink(Account account, Drink drink);

    Account checkout(CoffeeMachine coffeeMachine, Account account);

    Integer getCountDrinkInAccount(Account account, Drink drink);

    Integer getCountIngridientInAccount(Account account, Ingridient ingridient);
}
