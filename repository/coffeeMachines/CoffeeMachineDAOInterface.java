package com.epam.training.spring.core.demo.repository.coffeeMachines;

import com.epam.training.spring.core.demo.DTO.CoffeeMachine;
import com.epam.training.spring.core.demo.DTO.Drink;
import com.epam.training.spring.core.demo.DTO.Ingridient;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public interface CoffeeMachineDAOInterface {
    void addCoffeeMachine(HashMap<Drink, Integer> drinks, HashMap<Ingridient, Integer> ingridients);

    CoffeeMachine getCoffeeMachine(Integer idCoffeeMachine);

    void addDrink(CoffeeMachine coffeeMachine, Drink drink, Integer Portion);

    void setDrinkPortion(CoffeeMachine coffeeMachine, Drink drink, Integer Portion);

    Integer getDrinkPortion(CoffeeMachine coffeeMachine, Drink drink);

    List<Drink> getAllDrinks(CoffeeMachine coffeeMachine);

    void addIngridient(CoffeeMachine coffeeMachine, Ingridient ingridient, Integer portion);

    void setIngridientPortion(CoffeeMachine coffeeMachine, Ingridient ingridient, Integer portion);

    Integer getIngridientPortion(CoffeeMachine coffeeMachine, Ingridient ingridient);

    List<Ingridient> getAllIngridients(CoffeeMachine coffeeMachine);

    void showCoffeeMachine(CoffeeMachine coffeeMachine);
}
