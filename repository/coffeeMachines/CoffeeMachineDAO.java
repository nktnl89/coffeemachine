package com.epam.training.spring.core.demo.repository.coffeeMachines;

import com.epam.training.spring.core.demo.DTO.CoffeeMachine;
import com.epam.training.spring.core.demo.DTO.Drink;
import com.epam.training.spring.core.demo.DTO.Ingridient;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component("CoffeeMachineDAO")
public class CoffeeMachineDAO implements CoffeeMachineDAOInterface {
    List<CoffeeMachine> coffeeMachines = new ArrayList<>();

    @Override
    public void addCoffeeMachine(HashMap<Drink, Integer> drinks, HashMap<Ingridient, Integer> ingridients) {
        coffeeMachines.add(new CoffeeMachine(drinks, ingridients));
    }

    @Override
    public CoffeeMachine getCoffeeMachine(Integer idCoffeeMachine) {
        return coffeeMachines.get(idCoffeeMachine);
    }

    @Override
    public void addDrink(CoffeeMachine coffeeMachine, Drink drink, Integer portion) {
        Map<Drink, Integer> tmpDrinks = coffeeMachine.getDrinks();
        tmpDrinks.put(drink, portion);
        coffeeMachine.setDrinks(tmpDrinks);
    }

    @Override
    public void setDrinkPortion(CoffeeMachine coffeeMachine, Drink drink, Integer portion) {
        Map<Drink, Integer> tmpDrinks = coffeeMachine.getDrinks();
        for (Map.Entry<Drink, Integer> entry : tmpDrinks.entrySet()) {
            if (entry.getKey().getName() == drink.getName()) {
                entry.setValue(portion);
                break;
            }
        }
        coffeeMachine.setDrinks(tmpDrinks);
    }

    @Override
    public Integer getDrinkPortion(CoffeeMachine coffeeMachine, Drink drink) {
        Map<Drink, Integer> tmpDrinks = coffeeMachine.getDrinks();
        Integer portion = 0;
        for (Map.Entry<Drink, Integer> entry : tmpDrinks.entrySet()) {
            if (entry.getKey().equals(drink)) {
                portion = entry.getValue();
                break;
            }
        }
        return portion;
    }

    @Override
    public List<Drink> getAllDrinks(CoffeeMachine coffeeMachine) {
        List<Drink> result = null;
        Map<Drink, Integer> tmpDrinks = coffeeMachine.getDrinks();
        for (Map.Entry<Drink, Integer> entry : tmpDrinks.entrySet()) {
            result.add(entry.getKey());
        }
        return result;
    }

    @Override
    public void addIngridient(CoffeeMachine coffeeMachine, Ingridient ingridient, Integer portion) {
        Map<Ingridient, Integer> tmpIngridients = coffeeMachine.getIngridients();
        tmpIngridients.put(ingridient, portion);
        coffeeMachine.setIngridients(tmpIngridients);
    }

    @Override
    public void setIngridientPortion(CoffeeMachine coffeeMachine, Ingridient ingridient, Integer portion) {
        Map<Ingridient, Integer> tmpIngridient = coffeeMachine.getIngridients();
        for (Map.Entry<Ingridient, Integer> entry : tmpIngridient.entrySet()) {
            if (entry.getKey().equals(ingridient)) {
                entry.setValue(portion);
                break;
            }
        }
        coffeeMachine.setIngridients(tmpIngridient);
    }

    @Override
    public Integer getIngridientPortion(CoffeeMachine coffeeMachine, Ingridient ingridient) {
        Map<Ingridient, Integer> tmpIngridient = coffeeMachine.getIngridients();
        Integer portion = 0;
        for (Map.Entry<Ingridient, Integer> entry : tmpIngridient.entrySet()) {
            if (entry.getKey().equals(ingridient)) {
                portion = entry.getValue();
                break;
            }
        }
        return portion;
    }

    @Override
    public List<Ingridient> getAllIngridients(CoffeeMachine coffeeMachine) {
        List<Ingridient> result = null;
        Map<Ingridient, Integer> tmpIngridient = coffeeMachine.getIngridients();
        for (Map.Entry<Ingridient, Integer> entry : tmpIngridient.entrySet()) {
            result.add(entry.getKey());
        }
        return result;
    }

    @Override
    public void showCoffeeMachine(CoffeeMachine coffeeMachine) {
        System.out.println("------------------");
        System.out.println("Кофемашина:");
        for (Map.Entry<Drink, Integer> entry : coffeeMachine.getDrinks().entrySet()) {
            System.out.println(entry);
        }
        for (Map.Entry<Ingridient,Integer> entry:coffeeMachine.getIngridients().entrySet()){
            System.out.println(entry);
        }
    }
}
