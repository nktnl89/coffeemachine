package com.epam.training.spring.core.demo.repository.drinks;


import com.epam.training.spring.core.demo.DTO.Drink;

import java.util.List;

public interface DrinkDAOInterface {
    List<Drink> getAllDrinks();
    Drink addDrink(String name);
    Drink getDrink(String name);
}
