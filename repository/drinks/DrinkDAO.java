package com.epam.training.spring.core.demo.repository.drinks;

import com.epam.training.spring.core.demo.DTO.Drink;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component("DrinkDAO")
public class DrinkDAO implements DrinkDAOInterface {
    List<Drink> drinks = new ArrayList<>();

    @Override
    public List<Drink> getAllDrinks() {
        return drinks;
    }

    @Override
    public Drink addDrink(String name) {
        Drink newDrink = new Drink(name);
        drinks.add(newDrink);
        return newDrink;
    }

    @Override
    public Drink getDrink(String name) {
        for (Drink entryDrink : drinks) {
            if (entryDrink.getName() == name) {
                return entryDrink;
            }
        }
        return null;
    }
}
