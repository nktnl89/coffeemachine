package com.epam.training.spring.core.demo;

import com.epam.training.spring.core.demo.DTO.Account;
import com.epam.training.spring.core.demo.DTO.CoffeeMachine;
import com.epam.training.spring.core.demo.DTO.Drink;
import com.epam.training.spring.core.demo.DTO.Ingridient;
import com.epam.training.spring.core.demo.repository.accounts.AccountDAO;
import com.epam.training.spring.core.demo.repository.coffeeMachines.CoffeeMachineDAO;
import com.epam.training.spring.core.demo.repository.coffeeMachines.CoffeeMachineDAOInterface;
import com.epam.training.spring.core.demo.repository.drinks.DrinkDAO;
import com.epam.training.spring.core.demo.repository.ingridients.IngridientDAO;
import com.epam.training.spring.core.demo.repository.drinks.DrinkDAOInterface;
import com.epam.training.spring.core.demo.repository.ingridients.IngridientDAOInterface;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import java.util.HashMap;

@SpringBootApplication
public class DemoApplication {

    public static void main(String[] args) {
        org.springframework.context.ApplicationContext ctx = SpringApplication.run(DemoApplication.class, args);

        AccountDAO accountDAO = ctx.getBean(AccountDAO.class);
        Account tmpAccount = accountDAO.addAccount();

        DrinkDAOInterface drinkDAO = ctx.getBean(DrinkDAO.class);
        drinkDAO.addDrink("Американо");
        drinkDAO.addDrink("Эспрессо");
        drinkDAO.addDrink("3В1");

        //заполняем по две порции каждого кофе
        HashMap<Drink, Integer> coffeeMachineDrinks = new HashMap<>();
        for (Drink drink : drinkDAO.getAllDrinks()) {
            coffeeMachineDrinks.put(drink, 2);
        }

        IngridientDAOInterface ingridientDAO = ctx.getBean(IngridientDAO.class);
        ingridientDAO.addIngridient(new Ingridient("Сливки"));
        ingridientDAO.addIngridient(new Ingridient("Ванилька"));

        //заполняем по две порции ингридиентов
        HashMap<Ingridient, Integer> coffeeMachineIngridients = new HashMap<>();
        for (Ingridient ingridient : ingridientDAO.getAllIngridients()) {
            coffeeMachineIngridients.put(ingridient, 2);
        }

        //теперь у нас кофемашина с 2 порциями 3-х напитков и 2 порциями 2-х ингридиентов
        CoffeeMachineDAOInterface coffeeMachineDAO = ctx.getBean(CoffeeMachineDAO.class);
        coffeeMachineDAO.addCoffeeMachine(coffeeMachineDrinks, coffeeMachineIngridients);
        CoffeeMachine tmpCoffeeMachine = coffeeMachineDAO.getCoffeeMachine(0);

        //тут добавляем напитки в счет
        accountDAO.addDrink(tmpAccount, tmpCoffeeMachine, drinkDAO.getDrink("Американо"));
        accountDAO.addDrink(tmpAccount, tmpCoffeeMachine, drinkDAO.getDrink("Американо"));
        accountDAO.addDrink(tmpAccount, tmpCoffeeMachine, drinkDAO.getDrink("Американо"));
        //добавляем сливки во второй американо
        accountDAO.addIngridient(tmpAccount, tmpCoffeeMachine, 1, ingridientDAO.getIngridient("Сливки"));
        //оформляем счет
        accountDAO.checkout(tmpCoffeeMachine,tmpAccount);
        coffeeMachineDAO.showCoffeeMachine(tmpCoffeeMachine);

        ((ConfigurableApplicationContext) ctx).close();
    }
}
